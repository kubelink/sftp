#!/bin/sh

# set defaults

echo "Add current user to /etc/passwd file"
if ! whoami &> /dev/null; then
  if [ -w /etc/passwd ]; then
    echo "$USERNAME:x:$(id -u):0:sftp user:/home/$USERNAME:/sbin/nologin" >> /etc/passwd
  fi
fi

printf "Setup authorized keys \n"
HOME=/home/$USERNAME
echo "Home folder is $HOME"
mkdir -p $HOME/.ssh
chown $USERNAME:0 $HOME/.ssh
echo -e $SSH_AUTHORIZED_KEYS > $HOME/.ssh/authorized_keys
chmod -R 700 $HOME
chmod 600 $HOME/.ssh/authorized_keys

printf "Run sshd \n"
mkdir -p /var/run/sshd
ssh-keygen -A
exec /usr/sbin/sshd -D -e
