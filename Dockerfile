FROM centos:8

# Environment
ENV APP_ROOT /webroot
ENV PS1 "\n\n>> php \W \$ "
ENV ONDOCKER 1
WORKDIR $APP_ROOT
ENV PATH $APP_ROOT/bin:$PATH
ENV TERM=xterm
env USERNAME=sftp

## Packages
#RUN yum -y install epel-release && \
#    yum -y clean all

# Packages
ENV PACKAGES \
    openssh-server

RUN yum -y --setopt=tsflags=nodocs install $PACKAGES && \
    yum clean all


## Entrypoint
COPY entrypoint-sftp.sh /usr/local/bin/entrypoint-sftp.sh
COPY sshd_config /etc/ssh/sshd_config


# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

## Set permissions for application
RUN chown -R 1001:root -R /etc/ssh /usr/sbin/sshd /var/log /home /run
RUN chmod u+x  /usr/local/bin/entrypoint-sftp.sh /home /run
RUN chmod -R g=u /etc/passwd /etc/ssh /var/log /home /run

# Ready
USER 1001
EXPOSE 2103
CMD ["/usr/local/bin/entrypoint-sftp.sh"]